using DelimitedFiles
using JLD

frequencies = readdlm("elmerfem/frequencies.txt", ' ')[:]
response1 = im * zeros(length(frequencies))
response2 = im * zeros(length(frequencies))

for n in 1:length(frequencies)
    data1 = readdlm("elmerfem/probe1_$(n - 1).csv", ',')
    response1[n] = data1[2, 1] + im * data1[2, 2]
    
    data2 = readdlm("elmerfem/probe2_$(n - 1).csv", ',')
    response2[n] = data2[2, 1] + im * data2[2, 2]
end

save(
    "response.jld", 
    "frequencies", frequencies, 
    "response1", response1,
    "response2", response2
    )

