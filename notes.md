
Making the frequency axis:

```julia
# cd into the home-studio-4 project
include("fea-tools/tools.jl")
f = read_fem_eigenfrequencies("elmerfem/eigenvalues.dat")
F = 1:125
d = sort(unique(vcat(f, F)))
d = transpose(d[d .> 0])
length(d)
```

Use this to save an array of frequencies to text to copy and paste into the SIF file:

```julia
# cd into the home-studio-5 project
using DelimitedFiles
writedlm("elmerfem/frequencies.txt", array, ' ')
```

Reading Back the frequencies:

```julia
d = readdlm("elmerfem/frequencies.txt", ' ')
```
